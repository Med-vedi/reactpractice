import React, { Component } from "react";
import * as firebase from "firebase";
import HeaderBlock from "./components/HeaderComponents/index";
import Header from "./components/Header";
import Paragraph from "./components/Paragraph";
import { ReactComponent as LogoSvg } from "./logo.svg";
import Footer from "./components/Footer";
import CardList from "./components/CardList/CardList";
import firebaseConfig from "./components/services/firebaseDictionary";

firebase.initializeApp(firebaseConfig);

const database = firebase.database();

class App extends Component {
  state = {
    wordArr: [],
  };

  // constructor() {
  //   super();
  // }
  componentDidMount() {
    database
      .ref("/cards/")
      .once("value")
      .then((res) => {
        console.log("####: res", res.val());
        this.setState(
          {
            wordArr: res.val(),
          },
          this.setNewWord
        );
      });
  }

  setNewWord = () => {
    //   database.ref("/cards/" + this.state.wordArr.length).set({
    //     eng: eng,
    //     rus: rus,
    //     id: id,}
    // //     // eng: "mouse",
    // //     // rus: "мышь",
    // //     // id: Math.floor(Math.random() * Math.floor(100)),
    //   );
  };

  handleDeletedItem = (id) => {
    this.setState(({ wordArr }) => {
      const idx = wordArr.findIndex((item) => item.id === id);
      const newWordArr = [...wordArr.slice(0, idx), ...wordArr.slice(idx + 1)];
      return {
        wordArr: newWordArr,
      };
    });
  };
  render() {
    const { wordArr } = this.state;
    // console.log(wordArr)

    return (
      <div>
        <HeaderBlock>
          <Header>Время учить слова онлайн</Header>
          <Paragraph>
            Используйте карточки для запоминания и пополняйте активный словарный
            запас
          </Paragraph>
        </HeaderBlock>
        <CardList onDeletedItem={this.handleDeletedItem} item={wordArr} />

        <HeaderBlock hideBackground>
          <Header>Еще один заголовок</Header>
          <Paragraph>Здорово!</Paragraph>
          <LogoSvg />
        </HeaderBlock>
        <Footer />
      </div>
    );
  }
}

export default App;
