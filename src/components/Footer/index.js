import React from "react";
import s from "./Footer.module.scss";
import twitterLogo from "./img/Twitter_Rounded_Solid.png";
import googleLogo from "./img/Google_Rounded_Solid.png";
import facebookLogo from "./img/Facebook_Rounded_Solid.png";
import linkedLogo from "./img/LinkedIn_Rounded_Solid.png";

const Footer = () => {
  //   console.log("####: props", props);

  return (
    <div className={s.footer_wrapper}>
      {/* <p>&copy; Copyright @Junaed 2016, All rights reserved.</p> */}
      <div className={s.social_links.wrap}>
        <img className={s.social_links} src={twitterLogo} alt="tw" />
        <img className={s.social_links} src={googleLogo} alt="tw" />
        <img className={s.social_links} src={facebookLogo} alt="tw" />
        <img className={s.social_links} src={linkedLogo} alt="tw" />
      </div>
    </div>
  );
};

export default Footer;
