import React from "react";
import s from './Paragraph.module.scss'

const Paragraph = ({children}) => {

  return(
    <>
    <p className={s.par }>{children}</p>


    </>

  )
}

export default Paragraph;
