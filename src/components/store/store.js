const store = [
  {
    eng: "between",
    rus: "между",
    id: 0,
  },
  {
    eng: "high",
    rus: "высокий",
    id: 1,
  },
  {
    eng: "really",
    rus: "действительно",
    id: 2,
  },
  {
    eng: "something",
    rus: "что-нибудь",
    id: 3,
  },
  {
    eng: "most",
    rus: "большинство",
    id: 4,
  },
  {
    eng: "another",
    rus: "другой",
    id: 5,
  },
  {
    eng: "much",
    rus: "много",
    id: 6,
  },
  {
    eng: "family",
    rus: "семья",
    id: 7,
  },
  {
    eng: "own",
    rus: "личный",
    id: 8,
  },
  {
    eng: "out",
    rus: "из/вне",
    id: 9,
  },
  {
    eng: "leave",
    rus: "покидать",
    id: 10,
  },
];

export default store;
