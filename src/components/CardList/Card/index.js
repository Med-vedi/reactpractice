import React from "react";
import cl from "classnames";
import s from "./Card.module.scss";
import { CheckSquareOutlined, DeleteOutlined } from "@ant-design/icons";

class Card extends React.Component {
  state = {
    done: false,
    isRemembered: false,
  };

  handleCardClick = () => {
    this.setState(({ done }) => {
      return {
        done: !done,
      };
    });
  };

  handleIsRememberedClick = (e) => {
    e.preventDefault();
    this.setState(({ isRemembered }) => {
      return {
        isRemembered: !isRemembered,
      };
    });
  };
  handleDeletedClick = () => {
    this.props.onDeleted()
  }

  render() {
    const { eng, rus } = this.props;
    const { done, isRemembered } = this.state;

    return (
      <div className={s.root}>
        <div
          onClick={this.handleCardClick}
          className={cl(s.card, {
            [s.done]: done,
            [s.isRemembered]: isRemembered,
          })}
        >
          <div className={s.cardInner}>
            <div className={s.cardFront}>{eng}</div>
            <div className={s.cardBack}>{rus}</div>
            {/* <CheckSquareOutlined className={s.check} /> */}
          </div>
        </div>
        <div className={s.icons}>
          <CheckSquareOutlined onClick={this.handleIsRememberedClick} />
        </div>
        <div className={s.icons}>
        <DeleteOutlined onClick={this.handleDeletedClick}/>
        </div>
      </div>
    );
  }
}

export default Card;
