import React, { Component } from "react";
import Card from "./Card/index";
import s from "./CardList.module.scss";
import store from "../store/store";
import { Input, Button } from "antd";
import getTranslateWord from "../services/yandexDictionary";


class CardList extends Component {
  state = {
    value: " ",
    label: " ",
    isBusy: false,
    wordArr: [],
    store: store,
  };
  handleInputChange = (e) => {
    // console.log("///",e.target.value)

    this.setState({ value: e.target.value });
  };

  getWord = async () => {
    const { value } = this.state;

    const getWord = await getTranslateWord(value);
    const translatedWord = getWord[0].tr[0].text;

    this.setState(({ value }) => {
      return {
        label: `${value} - ${translatedWord}`,
        value: " ",
        isBusy: false,
      };
    });
  };

  handleAddItem = () => {
    let wordArr = this.props.item

    let newEng = this.state.label.split("-")[0];
    let newRus = this.state.label.split("-")[1];

    this.setState((e) => {

      const maxId = Math.max(...wordArr.map((item) => item.id)) + 1;

      const newItem = {
        eng: newEng,
        rus: newRus,
        id: maxId,
      };
      const newWordList = wordArr.push(newItem);

      return {
        newWordArr: newWordList,
      };
    });
  };

  handleSubmitForm = async () => {
    this.setState(
      {
        isBusy: true,
      },
      this.getWord
    );
  };

  // onAddWord = () => {
  //   const maxId = Math.max(...store.map((item) => item.id)) + 1;
  //   store.push({ id: maxId, rus: this.state.label, eng: this.state.value });
  //   // console.log(`Ты ввел новое слово: ${this.state.value}`);
  // };

  render() {
    const { Search } = Input;
    const { value, label, isBusy } = this.state;
    const { item = [], onDeletedItem } = this.props;
    console.log('...',item)
    return (
      <div className={s.cards_wrapper}>
        <div className={s.label}>{label}</div>
        <div className={s.form}>
          <Search
            placeholder="input search text"
            enterButton="Search"
            loading={isBusy}
            size="large"
            value={value}
            onSearch={this.handleSubmitForm}
            onChange={this.handleInputChange}
            // setNewWord={this.handleAddItem}
          />
          <Button className={s.btn_save} onClick={this.handleAddItem} type="primary" danger>
          Save
        </Button>

        </div>
        
        <div className={s.root}>
          {item.map(({ eng, rus, id }) => (
            <Card
              item={this.props.item}
              onDeleted={() => onDeletedItem(id)}
              key={id}
              eng={eng}
              rus={rus}

            />
          ))}
        </div>
      </div>
    );
  }
}

export default CardList;
